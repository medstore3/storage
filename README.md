<img src="https://cdn4.iconfinder.com/data/icons/logos-3/456/nodejs-new-pantone-black-512.png" align="right" width="50px">

<br><br>

# Storage - Aplicação Nest.js

Esta é uma aplicação Node.js e TypeScript, utilizando o framework Nest.js, que armazena os cálculos do Medstore, utilizando o conceito de filas.

## Ambiente de Desenvolvimento

### Pré-requisitos

Certifique-se de ter as seguintes ferramentas instaladas em sua máquina de desenvolvimento:

- Docker
- Docker Compose

### Passo a Passo

1. Clone este repositório em sua máquina local:

```bash
git clone <url_do_repositorio>
```

2. Acesse o diretório raiz do projeto:

```bash
cd <pasta_do_projeto>
```

3. Abra com o Visual Studio Code:

```bash
code .
```

4. Clique no botão: Abrir em um container:

<img src="https://code.visualstudio.com/assets/docs/devcontainers/create-dev-container/dev-container-reopen-prompt.png">

## Funcionamento

A aplicação utiliza uma fila para receber os resultados dos cálculos gerados por outros microserviços e salvar em uma base de dados SQLite, usando o ORM Prisma.

A fila é processada de forma assíncrona, ou seja, a aplicação pode salvar vários cálculos simultaneamente. Assim que um cálculo é concluído, o resultado é notificado através de um tópico, que é replicado na fila deste serviço, para que possa ser armazenado.

## Contribuição

Contribuições são bem-vindas! Se você quiser adicionar novas rotas ou melhorar as existentes, sinta-se à vontade para enviar um pull request para o repositório da biblioteca no GitLab. Certifique-se de seguir as diretrizes de contribuição e de teste fornecidas no projeto.

## Licença

Esta biblioteca está licenciada sob a **Licença MIT**. Sinta-se à vontade para usá-la em seus projetos comerciais ou pessoais.

<img src="https://cdn4.iconfinder.com/data/icons/logos-3/456/nodejs-new-pantone-black-512.png" width="50px">
