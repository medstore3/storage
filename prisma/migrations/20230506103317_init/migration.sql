-- CreateTable
CREATE TABLE "Calculations" (
    "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "name" TEXT NOT NULL,
    "result" REAL NOT NULL
);
