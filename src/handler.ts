import { Injectable } from "@nestjs/common";
import { SqsMessageHandler } from "@ssut/nestjs-sqs";
import * as AWS from "aws-sdk";
import { calcStorageConfig } from "./config";
import { CalculationStorageService } from "./calculation-storage.service";
import { API_DATA } from "@medstore/common-types";

@Injectable()
export class Handler {
  constructor(private readonly storage: CalculationStorageService) {}

  @SqsMessageHandler(calcStorageConfig.QUEUE_NAME, false)
  async handleMessage(message: AWS.SQS.Message) {
    const dataToStore = this.hydrateReceivedBody(message.Body);
    await this.storage.store(dataToStore);
  }

  private hydrateReceivedBody(body: string): API_DATA.CalculatedValue {
    return JSON.parse(body);
  }
}
