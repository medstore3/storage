import { NestFactory } from "@nestjs/core";
import * as AWS from "aws-sdk";
import { config } from "./config";
import { AppModule } from "./app.module";

// eslint-disable-next-line @typescript-eslint/no-var-requires
require("aws-sdk/lib/maintenance_mode_message").suppress = true;

async function bootstrap() {
  console.log(`
  ███████╗████████╗ ██████╗ ██████╗  █████╗  ██████╗ ███████╗
  ██╔════╝╚══██╔══╝██╔═══██╗██╔══██╗██╔══██╗██╔════╝ ██╔════╝
  ███████╗   ██║   ██║   ██║██████╔╝███████║██║  ███╗█████╗  
  ╚════██║   ██║   ██║   ██║██╔══██╗██╔══██║██║   ██║██╔══╝  
  ███████║   ██║   ╚██████╔╝██║  ██║██║  ██║╚██████╔╝███████╗
  ╚══════╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝ ╚═════╝ ╚══════╝
`);

  AWS.config.update({
    region: config.AWS_REGION,
    accessKeyId: config.ACCESS_KEY_ID,
    secretAccessKey: config.SECRET_ACCESS_KEY,
  });

  const app = await NestFactory.create(AppModule);
  await app.listen(3000);
}
bootstrap();
