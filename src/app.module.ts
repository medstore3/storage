import { Module } from "@nestjs/common";
import * as AWS from "aws-sdk";
import { config } from "./config";
import { Handler } from "./handler";
import { CalculationStorageService } from "./calculation-storage.service";
import { CalcSqsModule } from "./calc.sqs.module";
import { PrismaModule } from "./prisma.module";
import { MainController } from "./controller";

AWS.config.update({
  region: config.AWS_REGION,
  accessKeyId: config.ACCESS_KEY_ID,
  secretAccessKey: config.SECRET_ACCESS_KEY,
});

@Module({
  imports: [PrismaModule, CalcSqsModule],
  providers: [Handler, CalculationStorageService],
  controllers: [MainController],
})
export class AppModule {}
