import { SqsModule } from "@ssut/nestjs-sqs";
import { config, calcStorageConfig } from "./config";

export const CalcSqsModule = SqsModule.register({
  consumers: [
    {
      name: calcStorageConfig.QUEUE_NAME,
      queueUrl: calcStorageConfig.QUEUE_URL,
      region: config.AWS_REGION,
    },
  ],
});
