import { Controller, Get } from "@nestjs/common";
import { CalculationStorageService } from "./calculation-storage.service";

@Controller()
export class MainController {
  constructor(private readonly storage: CalculationStorageService) {}

  @Get("calculations")
  async getCalculations(): Promise<any> {
    return {
      data: await this.storage.getAll(),
    };
  }
}
