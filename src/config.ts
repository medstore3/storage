export const config = {
  AWS_REGION: "us-east-1",
  ACCESS_KEY_ID: "test",
  SECRET_ACCESS_KEY: "test",
};

export const calcStorageConfig = {
  QUEUE_NAME: "http://localstack:4566/000000000000/CalcDataToStorage",
  QUEUE_URL: "http://localstack:4566/000000000000/CalcDataToStorage",
};
