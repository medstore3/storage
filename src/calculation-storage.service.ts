import { Injectable } from "@nestjs/common";
import { PrismaService } from "./prisma.service";
import { API_DATA } from "@medstore/common-types";

@Injectable()
export class CalculationStorageService {
  constructor(private readonly prismaService: PrismaService) {}

  async store(data: API_DATA.CalculatedValue): Promise<void> {
    await this.prismaService.calculations.create({
      data: {
        name: data.name,
        result: data.value,
      },
    });
  }

  async getAll(): Promise<any> {
    return await this.prismaService.calculations.findMany();
  }
}
