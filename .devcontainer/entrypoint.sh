#!/bin/bash

echo 'Linking packages...';

cd /usr/app/.devcontainer/pkg_volumes/@medstore/common-types;
npm link;

cd /usr/app;
npm link @medstore/common-types;

echo 'Linking packages was finished!';

find /usr/app/node_modules -maxdepth 0 -empty -exec npm install \;

npm run build:dev &

pm2-dev /usr/app/ecosystem.config.js;
